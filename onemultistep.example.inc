<?php

function onemultistep_example($node = NULL) {
//lap_dpm($_POST, '$_POST');

  $form = array();
  $form_state = array();
  // Send defaults through 'values'
  //$form_state['values']['node_bundle'] = $node ? $node->type : NULL;
  $form_state['build_info']['args'] = array(array('node_bundle' => $node ? $node->type : NULL));
  $form = drupal_build_form('onemultistep_example_form', $form_state);

  //$form = drupal_render($form);
  return $form;
}


function onemultistep_example_form($form, &$form_state, $args = NULL) {
//lap_dpm($form_state, 'FORM FUNCTION $form_state');

//    'group_measurements' => array(
//      'group_name' => 'group_measurements',
//      'label' => 'Measurements',
//      'format_type' => 'multipage',
//      'children' => array (
//        'number',
//      ),
//    ),
//    'group_goal' => array(
//      ...
//    ),
//    ),
//    'group_steps' => array(
//      'group_name' => 'group_steps',
//      'label' => 'Steps',
//      'format_type' => 'multipage-group',
//      'children' => array (
//        'group_measurements',
//        'group_goal',
//      ),
//      'format_settings' => array (
//        'label' => 'Steps',
//        'instance_settings' => array (
//          'classes' => ' group-steps field-group-multipage-group',
//          'page_header' => '3',
//          'page_counter' => '1',
//          'move_button' => '1',
//          'move_additional' => '1',
//        ),
//      ),
//    ),
//  );


  onemultistep_start($form, $form_state);
  

  onemultistep_set_step($form, 'A', 'Step A');

  $element_name = 'date';
  $element_key = $element_name;
  $element  = array(
    '#type' => 'date',
    '#title' => 'Date',
    '#default_value' => array('year' => 2007, 'month' => 2, 'day' => 15),
    '#description' => 'A date field',
  );
  $element['#required'] = FALSE;
  // if you don't set '#default_value', a disabled element will show nothing
  $value = onemultistep_builder_value($form_state, $element_key, $element);
  $element['#default_value'] = $value; 
  $form[$element_name] = $element;


  $form['container'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $element_name = 'number';
  $element_key = 'container][' . $element_name;
  $element = array(
    '#title' => 'number',
    '#type' => 'textfield',
    '#description' => t ('A real number required with a parent container and with tree on'),
    '#size' => 5,
    //'#tree' => FALSE,
  );
  $element['#required'] = TRUE;
  // if you don't set '#default_value', a disabled element will show nothing
  $value = onemultistep_builder_value($form_state, $element_key, $element);
  $element['#default_value'] = $value; 
  $element['#step'] = 'A';
  $form['container'][$element_name] = $element;
  onemultistep_depends($form, $element_key, array('date'));


  $element_key = 'number2';
  $element_key = $element_name;
  //$value = isset($form_state['values'][$element_key]) ? $form_state['values'][$element_key] : null;
  $element = array(
    '#title' => 'number2',
    '#type' => 'textfield',
    '#description' => t ('A real number required between 0 and 20'),
    '#size' => 5,
    '#rules' => array('decimal[2,2]', 'range[0,20]'),
  );
  $element['#required'] = TRUE;
  // if you don't set '#default_value', a disabled element will show nothing
  $value = onemultistep_builder_value($form_state, $element_key, $element);
  $element['#default_value'] = $value; 
  $form[$element_name] = $element;
  onemultistep_depends($form, $element_key, array('container][number'));
  
  
  onemultistep_set_step($form, 'B', 'Step B');

  $element_name = 'node_bundle';
  $element_key = $element_name;
  //$value = isset($form_state['values'][$element_key]) ? $form_state['values'][$element_key] : null;
  //$value = isset($args[$element_key]) ? $args[$element_key] : null;
  //$value = isset($form_state['input'][$element_key]) ? $form_state['input'][$element_key] : $value;
  $element = array(
    '#type' => 'select',
    '#title' => 'Node bundle: node_type_get_names()',
    //'#description' => t('Descriptive text under form elements rather than above them is a great UX choice, really.'),
    //'#ajax' => array(
      //'callback' => 'onemultistep_example_callback',
      //'event' => 'contextmenu change',
      //'event' => 'contextmenu',
      //'wrapper' => 'replace_field',
      //'callback' => 'ajax_example_dependent_dropdown_degrades_first_callback',
      //'wrapper' => 'dropdown-second-replace',
    //),
    //'#onemultistep_dependents' => array('field', 'entity_info', 'number'),
    '#default_value' => isset($args[$element_key]) ? $args[$element_key] : null,
  );
  $value = onemultistep_builder_value($form_state, $element_key, $element);
  $required = FALSE;
  //$required = TRUE;
  $options = node_type_get_names();
  //$options = ...;
  if ( $required ) {
    // Add any extra options you desire
    $options = array('*' => t('-Select All-')) + $options;

    $value = isset($options[$value]) ? $value : null; // NULL and '' will became NULL
    if ( !isset($value) ) {
      // If you want a custom Select, add it this way
      $element['#empty_option'] = t('-Select One, please-');
    }
  }
  if ( !$required ) {
    // Add any extra options you desire
    $options = array('*' => t('-All-')) + $options;
    // If !$required, don't set #empty_option or #empty_value
    // If you want a custom None, add it yourself. If you don't, first value will be selected.
    $options = array('' => t('-None-')) + $options; // '' key will NOT set an error on validation
    // Note: you could set $element['#empty_option'] = t('-Custom None-') instead. But you wont be able to
    // destinguish '' from NULL on form defaults (both will be set to NULL on $value check).

    $value = isset($options[$value]) ? $value : null; // NULL becames NULL and '' becames ''
    //$element['#default_value'] = ( is_null($value) || !isset($options[$value]) ) ? key($options) : $value; 
    //$element['#default_value'] = isset($options[$value]) ? $value : null; 
  }
  $element['#default_value'] = $value; 
  $element['#required'] = $required;
  $element['#options'] = $options;
  $element['#step'] = 'B';
  $form[$element_name] = $element;
  
  $bundle_value = $value;


  $element_name = 'field';
  $element_key = $element_name;
  //$value = isset($form_state['values'][$element_key]) ? $form_state['values'][$element_key] : null;
  $element = array(
    '#type' => 'select',
    '#title' => 'Field',
    //'#description' => t('.'),
    //'#ajax' => array(
    //  'callback' => 'vnode_onemultistep_callback',
    //),
    //'#onemultistep_dependents' => array('field_info_field'),
    //'#access' => FALSE,
  );
  $value = onemultistep_builder_value($form_state, $element_key, $element);
  //$required = TRUE;
  $required = FALSE;
  if ( !empty($bundle_value) && ($bundle_value != '*') ) {
    $fields = field_info_instances('node', $bundle_value);
    $options = array();
    foreach ($fields as $name => $val) { 
      $options[$name] = $name . ' (' . $val['label'] .')';
    }
  }
  else {
    $fields = field_info_field_map();
    $options = drupal_map_assoc(array_keys($fields));
  }
  $options = array('' => t('-None-')) + $options; // '' key will NOT set an error on validation

  $value = isset($options[$value]) ? $value : null; // NULL becames NULL and '' becames ''

  $element['#default_value'] = $value; 
  $element['#required'] = $required;
  $element['#options'] = $options;
  $element['#step'] = 'B';
  $form[$element_name] = $element;
  onemultistep_depends($form, $element_key, array('node_bundle'));

  $field_value = $value;


  $element_name = 'entity_info';
  $element_key = $element_name;
  $element = array('#type' => 'hidden');
  if ( !empty($bundle_value) ) {
    $entity_info = entity_get_info('node');
    if ( ($bundle_value == '*') ) {
      //$entity_info['bundles_value'] = 'removed...';
      $entity_info = var_export($entity_info, TRUE);
      $entity_info_title = 'entity_info(node)';
    }
    else {
      $entity_info = var_export($entity_info['bundles'][$bundle_value], TRUE);
      $entity_info_title = 'entity_info(node) [bundles][' . $bundle_value . ']';
    }
    $element = array(
      '#title' => $entity_info_title,
      '#type' => 'textarea',
      //'#description' => t ('The comment will be unpublished if it contains any of the phrases above. Use a case-sensitive, comma-separated list of phrases. Example: funny, bungee jumping, "Company, Inc."'),
      //'#disabled' => TRUE,
      '#attributes' => array('readonly' => 'readonly'),
    );
    //$form_state['input'][$element_name] = $entity_info;
    //$element['#default_value'] = $entity_info;
    $element['#value'] = $entity_info;
  }
  $element['#step'] = 'B';
  $form[$element_name] = $element;
  onemultistep_depends($form, $element_key, array('node_bundle'));


  $element_name = 'field_info_field';
  $element_key = $element_name;
  $element = array('#type' => 'hidden');
  if ( !empty($field_value) ) {
    $field_info_field = var_export(field_info_field($field_value), TRUE);
    $element = array(
      '#title' => 'field_info_field(' . $field_value . ')',
      '#type' => 'textarea',
      //'#description' => t (''),
      //'#rows' => 15,
      '#attributes' => array('readonly' => 'readonly'),
    );
    //$element['#default_value'] = $field_info_field;
    //$form_state['input'][$element_name] = $field_info_field;
    $element['#value'] = $field_info_field;
  }
  $element['#step'] = 'B';
  $form[$element_name] = $element;
  onemultistep_depends($form, $element_key, array('field'));


  $element_name = 'field_info_instance';
  $element_key = $element_name;
  if ( !empty($bundle_value) && !empty($field_value) ) {
    $field_info_instance = var_export(field_info_instance('node', $field_value, $bundle_value), TRUE);
    $field_info_instance_title = 'field_info_instance(node, ' . $field_value . ', ' . $bundle_value . ')';
  }
  else {
    //$field_info_instance = '';
    $field_info_instance = var_export(field_info_field_map(), TRUE);
    $field_info_instance_title = 'field_info_field_map()';
  }
  $element = array(
    '#title' => $field_info_instance_title,
    '#type' => 'textarea',
    //'#description' => t (''),
    //'#rows' => 15,
    '#attributes' => array('readonly' => 'readonly'),
  );
  //$element['#default_value'] = $field_info_instance;
  //$form_state['input'][$element_key] = $field_info_instance;
  $element['#value'] = $field_info_instance;
  $element['#step'] = 'B';
  $form[$element_name] = $element;
  onemultistep_depends($form, $element_key, array('node_bundle', 'field'));

  //onemultistep_start($form, $form_state, 'container2][div');
  //$onemultistep = onemultistep_start($form, $form_state);
  //$onemultistep['#js_use'] = $js_use;
  //onemultistep_depends($onemultistep, 'field_info_field', array('field'));

  //$form['actions'] = array(
  //  '#step' => 'B',
  //);
  //$form['actions']['test'] = array(
  //  '#type' => 'submit',
  //  '#name' => 'test',
  //  '#value' => 'Exit',
  //  '#submit' => array('onemultistep_example_form_submit'),
  //  //'#description' => t('Save'),
  //);

  onemultistep_set_button($form, 'submit',
    array(
      '#type' => 'submit',
      '#name' => 'test',
      '#value' => 'Exit',
      //'#submit' => array('onemultistep_example_form_submit'),
      //'#description' => t('Save'),
    )
  );


//lap_dpm($form, 'FORM FUNCTION (out) $form');

  return $form;
}


function onemultistep_example_callback(&$form, &$form_state) {
//lap_dpm($form, 'ONEMULTISTEP EXAMPLE CALLBACK $form');
//lap_dpm($form_state, 'ONEMULTISTEP EXAMPLE CALLBACK $form_state');
  //$commands[] = ajax_command_changed('#edit-node-bundle');
  $commands[] = ajax_command_changed('.form-item-node-bundle', 'label');
  onemultistep_default_commands($form, $form_state, $element, $commands);
//lap_dpm($commands, 'ONEMULTISTEP EXAMPLE CALLBACK $commands');
  return array('#type' => 'ajax', '#commands' => $commands);
}  


function onemultistep_example_form_validate($form, &$form_state) {
//lap_dpm('VALIDATE');
}


function onemultistep_example_form_submit($form, &$form_state) {
//lap_dpm($form_state['triggering_element']['#name'], 'SUBMIT $form_state[triggering_element][#name]');
//lap_dpm($form_state['build_info']['args'], 'SUBMIT $form_state[build_info][args]');
//lap_dpm($form_state, 'SUBMIT $form_state');
//lap_dpm($form, 'SUBMIT $form');
}


function onemultistep_example_dropdown() {
//lap_dpm($_POST, '$_POST');

  $form = drupal_get_form('onemultistep_example_dependent_dropdown');
  return $form;
}

function onemultistep_example_dependent_dropdown($form, &$form_state) {
  
  onemultistep_start($form, $form_state);

  $options_first = _onemultistep_example_get_first_dropdown_options();
  $element = array(
    '#type' => 'select',
    '#title' => 'Instrument Type',
    '#options' => $options_first,
    '#empty_value' => '',
    '#required' => TRUE,
  );
  $selected = onemultistep_builder_value($form_state, 'dropdown_first', $element);
  $element['#default_value'] = $selected; 
  $form['dropdown_first'] = $element;

  $element = array(
    '#type' => 'select',
    '#title' => (empty($selected) ? '' : $options_first[$selected] . ' ') . t('Instruments'),
    '#options' => _onemultistep_example_get_second_dropdown_options($selected),
    '#empty_value' => '',
    '#required' => TRUE,
  );
  $value = onemultistep_builder_value($form_state, 'dropdown_second', $element);
  $element['#default_value'] = $value;
  $form['dropdown_second'] = $element;

  if (empty($selected)) {
    $form['dropdown_second']['#description'] = t('You must make your choice on the first dropdown before changing this second one.');
  }

  onemultistep_depends($form, 'dropdown_second', array('dropdown_first'));

  onemultistep_set_button($form, 'submit',
    array(
      '#type' => 'submit',
      '#value' => t('OK'),
    )
  );

//lap_dpm($form, 'onemultistep_dependent_dropdown (out) $form');
  return $form;
}

function _onemultistep_example_get_first_dropdown_options() {
  return drupal_map_assoc(array(t('String'), t('Woodwind'), t('Brass'), t('Percussion')));
}

function _onemultistep_example_get_second_dropdown_options($key = '') {
  $options = array(
    t('String') => drupal_map_assoc(array(t('Violin'), t('Viola'), t('Cello'), t('Double Bass'))),
    t('Woodwind') => drupal_map_assoc(array(t('Flute'), t('Clarinet'), t('Oboe'), t('Bassoon'))),
    t('Brass') => drupal_map_assoc(array(t('Trumpet'), t('Trombone'), t('French Horn'), t('Euphonium'))),
    t('Percussion') => drupal_map_assoc(array(t('Bass Drum'), t('Timpani'), t('Snare Drum'), t('Tambourine'))),
  );
  if (isset($options[$key])) {
    return $options[$key];
  }
  else {
    return array();
  }
}

function onemultistep_example_dependent_dropdown_submit($form, &$form_state) {

  // Now handle the case of the next, previous, and submit buttons.
  switch ($form_state['triggering_element']['#value']) {
  case t('OK'): // Submit: We're done.
    drupal_set_message(t('Your values have been submitted. dropdown_first=@first, dropdown_second=@second', array('@first' => $form_state['values']['dropdown_first'], '@second' => $form_state['values']['dropdown_second'])));
    break;
  }
}
