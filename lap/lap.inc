<?php

function lap_dpm($input, $name = NULL, $type = 'status') {
  if( module_exists('devel') && !module_exists('devel_themer') ) {
    return dpm($input, $name, $type);
  }
  $message = (isset($name) ? $name . ' => ' : '') . var_export($input, TRUE);
  drupal_set_message($message, $type);
  return $input;
}

function lap_dpq($query, $return = FALSE, $name = NULL) {
  if( module_exists('devel') && !module_exists('devel_themer') ) {
    return dpq($query, $return, $name);
  }
  return '';
}


/*
 *  $items['examples/menu_example/custom_access/page'] = array(
 *    ...
 *    'access callback' => 'lapccmec_hasrole',
 *    'access arguments' => array(array('role1', 'role2')),
 */
function lap_hasRole($role_names, $account = null){
  if (empty($account)) {
    $account = $GLOBALS['user'];
  }

  $access_granted = false;
  foreach ($role_names as $role_name) {
    $access_granted = ($access_granted or in_array($role_name, $account->roles));
  }
  return $access_granted;
}


/*
 * $a['key'] = array('a');
 * lap_array_akey_merge($a, 'key', array('b','c')); // $a['key'] -> array('a', array('b','c'));
 * @see array_merge
 */
function lap_array_akey_merge(&$array, $key, $add, $unshift = null) {
  if ( !isset($array[$key]) || !is_array($array[$key]) ) {
    if ( !isset($array[$key]) ) {
      $array[$key] = array();
    }
    else {
      $array[$key] = array();
      //return;
    }
  }  
  if ( (bool) $unshift ) {
    $array[$key] = array_merge(array($add), $array[$key]);
  }
  else {
    $array[$key] = array_merge($array[$key], array($add));
  }  
}


function lap_array_akey_merge_unique(&$array, $key, $add, $unshift = null) {
  if ( !isset($array[$key]) || !is_array($array[$key]) || empty($array[$key]) ) {
    lap_array_akey_merge($array, $key, $add, $unshift);
  }
  else {
    if ( !in_array($add, $array[$key], TRUE) ) {
      lap_array_akey_merge($array, $key, $add, $unshift);
    }  
  }  
}


/*
 * $a['key'] = 'a';
 * lap_array_skey_merge($a, 'key', 'b'); // $a['key'] -> 'ab';
 */
function lap_array_skey_merge(&$array, $key, $add, $unshift = null) {
  if ( !isset($array[$key]) || !is_string($array[$key]) ) {
    if ( !isset($array[$key]) ) {
      $array[$key] = '';
    }
    else {
      return;
    }
  }  
  $add = is_string($add) ? $add : '';
  if ( (bool) $unshift ) {
    $array[$key] = $add . $array[$key];
  }
  else {
    $array[$key] .= $add;
  }  
}



/*
 *
 */
function laptools_theme($existing, $type, $theme, $path) {
//lap_dpm($existing, 'lap_ccmec_theme $existing');
//lap_dpm($type, 'lap_ccmec_theme $type');
//lap_dpm($theme, 'lap_ccmec_theme $theme');
//lap_dpm($path, 'lap_ccmec_theme $path');
  return array(
    'lap_form_table_bycolumns' => array(
      'render element' => 'element',
    ),
  );
}


/*
 *
 */
function theme_lap_form_table_bycolumns(&$vars) {
  $element = $vars['element'];

//lap_dpm($element, 'theme_lap_ccmec_form_table_bycolumns $element');

  $element['#rows'] = array();
  foreach (element_children($element) as $key) {
    $c = array();
    if ( isset($element[$key]['#type']) && ($element[$key]['#type'] == 'value') ) {
      if ( isset($element[$key]['#form_table']) ) {
        foreach ($element[$key]['#value'] as $val) {
          $cell = ( is_array($val) && key_exists('data', $val) ) ? $val : array('data' => $val);
          $cell += $element[$key]['#form_table'];
          $c[] = $cell;
        } unset($val);
      } else {
        $c = $element[$key]['#value'];
      }
         
    } else {
      if ( isset($element[$key]['#form_table']) ) {
        foreach (element_children($element[$key]) as $c_key) {
          $cell = array(
            'data' => array( $c_key => $element[$key][$c_key]),
          );
          $cell += $element[$key]['#form_table'];
          $c[] = $cell;
        } unset($c_key);
      } else {
        foreach (element_children($element[$key]) as $c_key) {
          $cell = array(
            'data' => array( $c_key => $element[$key][$c_key]),
          );
          $c[] = $cell;
        } unset($c_key);
      }

    }

    if ( isset($element['#header']) ) {
      $element['#header'][] = ( isset($element[$key]['#header']) ) ? $element[$key]['#header'] : '';
    }

//lap_dpm($c, 'theme_lap_ccmec_form_table_bycolumns $c');

    lap_form_table_addColumn($element, $c);
  } unset($key);

//lap_dpm($element, 'theme_lap_ccmec_form_table_bycolumns $element');

  return theme('table', array('header' => $element['#header'], 'rows' => $element['#rows'], 'caption' => $element['#caption']));
  //return theme('table', array('header' => array(), 'rows' => $element['#rows'], 'caption' => $element['#caption']));
}


/*
 *
 */
function lap_form_table_addColumn(&$t, $c) {

//lap_dpm($t, 'lap_ccmec_jurado_preavaliacao_classificacao_element_addColumn $t');
//lap_dpm($c, 'lap_ccmec_jurado_preavaliacao_classificacao_element_addColumn $c');

  if ( !isset($t['#rows']) ) $t['#rows'] = array();
  
  reset($t['#rows']);
  reset($c);
  for($i = 0, $size = count($c); $i < $size; ++$i) {
    
    $cell = current($c);
    //if ( !(list($tkey, $row) = each($t['#rows'])) ) {
    if ( (current($t['#rows']) === false) ) {
      $t['#rows'][$i] = array('data' => array() );
      end($t['#rows']);
    }

    $tkey = key($t['#rows']);

//lap_dpm($tkey);

    $t['#rows'][$tkey]['data'][] = $cell;
    next($t['#rows']);
    next($c);
  }
    
//lap_dpm($t, 'lap_ccmec_jurado_preavaliacao_classificacao_element_addColumn $t (2)');
}


/**
 * Just an alias to node field_get_items().
 */
function lap_node_field_get_items($node, $field_name, $langcode = null) {
  return field_get_items('node', $node, $field_name, $langcode);
}


/**
 *
 * 
 * @see _field_invoke((), field_default_submit()
*/
function lap_node_field_set_items($node, $field_name, $items, $langcode = null) {
  $langcode = field_language('node', $node, $field_name, $langcode);
  
  $instance = field_info_instance('node', $field_name, $node->type);
  $field = field_info_field_by_id($instance['field_id']);

  // Filter out empty values.
  $items = _field_filter_items($field, $items);
  // Reorder items to account for drag-n-drop reordering.
  $items = _field_sort_items($field, $items);  

  // Populate $items back in the field values, but avoid replacing missing
  // fields with an empty array (those are not equivalent on update).
  if ($items !== array() || isset($node->{$field_name}[$langcode])) {
    $node->{$field_name}[$langcode] = $items;
  }

/*
  if ( is_null($items) ) {
    unset($node->{$field_name}[$langcode]);
  }
  else {
    $node->{$field_name}[$langcode] = $items;
  }
*/
}


/**
 *
 */
function lap_node_field_value($node, $field_name, $delta, $path_key, $langcode = null) {
  /*
  $langcode = field_language('node', $node, $field_name, $langcode);
  if ( isset($node->{$field_name}[$langcode][$delta][$path_key]) ) {
    return $node->{$field_name}[$langcode][$delta][$path_key];
  }
  else {
    return null;
  }
  */
  $items = lap_node_field_get_items($node, $field_name, $langcode);
  return $items ? $items[$delta][$path_key] : $items;
}


/**
 * Use 
 */
function lap_node_field_set_value($node, $field_name, $delta, $path_key, $value, $langcode = null) {
  $langcode = field_language('node', $node, $field_name, $langcode);
  if ( is_null($value) ) {
    unset($node->{$field_name}[$langcode][$delta]);
  }
  else {
    $node->{$field_name}[$langcode][$delta][$path_key] = $value;
  }
}


/**
 * No lang on items
 */
function lapccmecnode_vnode_vfield_get_items(&$vnode, $vfield_name, $langcode = null) {
  $vfield = &lapccmecnode_vnode_vfield($vnode, $vfield_name);

  if ( !$vfield ) {
    return false;
  }

  $fnode = &lapccmecnode_vnode_node($vnode, $vfield['#node_vname']);
  if ( empty($fnode['#node']) ) {
    return false;
  }

  $field_name = $vfield['#field_name'];
  return lap_node_field_get_items('node', $fnode['#node'], $field_name, $langcode);
}




