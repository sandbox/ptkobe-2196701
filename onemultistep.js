/*
 * @file
 * JavaScript for ajax_example.
 *
 * See @link ajax_example_dependent_dropdown_degrades @endlink for
 * details on what this file does. It is not used in any other example.
 */

(function($) {

  // Re-enable form elements that are disabled for non-ajax situations.
  //Drupal.behaviors.enableFormItemsForAjaxForms = {
  Drupal.behaviors.onemultistep = {
    attach: function() {
      // If ajax is enabled.
      if (Drupal.ajax) {
        //$('.enabled-for-ajax').removeAttr('disabled');
        $('.js-nostyle').removeAttr('style');
        $('.js-nodisabled').removeAttr('disabled');
        $('.js-show').show();
        $('.js-hide').hide();
      }
    }
  };

})(jQuery);
